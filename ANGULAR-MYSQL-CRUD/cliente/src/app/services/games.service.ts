import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { map, Observable } from 'rxjs';
import { Game } from '../interfaces/Game';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  API_URL = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getGames(): Observable<Game[]>{
    return this.http.get<Game[]>(`${this.API_URL}/games`).pipe(map((res: any)=> res));
  }

  getGame(id: string) {
    return this.http.get(`${this.API_URL}/games/${id}`);
  }

  deleteGame(id: string): Observable<Game[]> {
    return this.http.delete<Game[]>(`${this.API_URL}/games/${id}`);
  }

  saveGame(game: Game) {
    return this.http.post(`${this.API_URL}/games`, game)
  }

  updateGame(id: string | number | undefined, updatedGame: Game): Observable<Game> {
    return this.http.put(`${this.API_URL}/games/${id}`, updatedGame)
  }
}
