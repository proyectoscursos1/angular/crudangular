import { GamesService } from './../../services/games.service';
import { Component, HostBinding, OnInit } from '@angular/core';
import { Game } from 'src/app/interfaces/Game';


@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  // @HostBinding('class') classes = 'row';

  listGames: Game [] = [];

  constructor(private gamesService: GamesService) { }

  ngOnInit() {
    this.getGames();
  }

  getGames(){
    this.gamesService.getGames().subscribe(res => {
      this.listGames = res;
      console.log(this.listGames);
    }
  );
  }

  deleteGame(id: any){
    this.gamesService.deleteGame(id).subscribe(res => {
      console.log(res);
      this.getGames();
    }, error => {
      console.log(error);
    });
  }
  


}
