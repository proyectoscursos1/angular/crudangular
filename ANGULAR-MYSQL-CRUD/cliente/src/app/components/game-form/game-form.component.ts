import { GamesService } from './../../services/games.service';
import { Component, OnInit } from '@angular/core';
import { Game } from 'src/app/interfaces/Game';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.css']
})
export class GameFormComponent implements OnInit {

  game: Game = {
    id: 0,
    title: '',
    description: '',
    image: '',
    created_at: new Date()
  };

  edit: boolean = false;

  constructor(private gameService: GamesService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    /* Obtener los parametros de la ruta */
    const params = this.activatedRoute.snapshot.params;
    console.log(params);

    if (params['id']) {
      this.gameService.getGame(params['id']).subscribe(
        res => {
          console.log(res);
          this.game = res;
          this.edit = true;
        }
      )

    }
  }

  saveNewGame() {
    delete this.game.created_at;
    delete this.game.id;

    this.gameService.saveGame(this.game).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/games']);
      },
      err => console.log(err));
  }

  updateGame(){
    delete this.game.created_at;
    this.gameService.updateGame(this.game.id, this.game).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/games']);
      },
      err => console.log(err));
  }
}
